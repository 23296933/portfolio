// When scrolled, the function is enabled
window.onscroll = function() {
	scrollTopFunction()
};

// the function makes the button appear, when the user scrolled scrolled past 400px
// else the button goes away 
function scrollTopFunction() {
  if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
    document.getElementById("topIcon").style.display = "block";
  } else {
    document.getElementById("topIcon").style.display = "none";
  }
};

// When the button is clicked, it goes to the top
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
};
